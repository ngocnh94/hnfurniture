<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main><!-- #main -->

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<?php wp_footer(); ?>

</body>

<style>.float-contact {
position: fixed;
bottom: 20px;
left: 20px;
z-index: 99999;
}

.chat-face {
background: #125c9e;
border-radius: 20px;
padding: 0 18px;
color: white;
display: block;
margin-bottom: 6px;
}
.float-contact .hotline {
background: #d11a59!important;
border-radius: 20px;
padding: 0 18px;
color: white;
display: block;
margin-bottom: 6px;
}
.chat-zalo a, .chat-face a, .hotline a {
font-size: 15px;
color: white;
font-weight: 400;
text-transform: none;
line-height: 0;
}
@media (max-width: 549px){
.float-contact{
display:none
}
}</style>
<div class="float-contact">
<button class="chat-face">
<a href="https://www.facebook.com/noithathuengoc">Chat Facebook</a>
</button>
<button class="hotline">
<a href="tel:0979350562">Hotline: 0979350562</a>
</button>
</div>

</html>
