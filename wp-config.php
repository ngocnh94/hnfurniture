<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hnfurniture' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CWXfM7p0gw9iGsYtonthnlZemiMZ+dONEHX8iU6wUrojlw37ai41s+23NBoxSDN4BIyQjCsitqrSmCJK7Sa/Vw==');
define('SECURE_AUTH_KEY',  'l5KsVLKuSIqNWZjjCEgFzHP5Umii/sCl/woGekEKNDRaAGxIuRRYMr9oIjb9mzMnW9/1POEQxBjKcaqwk+631g==');
define('LOGGED_IN_KEY',    'tjJOQZb49ipHOfJELZvnvgJN/TGn1zgGlhPfeWjzVndNlInjSG/1skwZm6cz4nmPiJBy5k8rsED1yA0vAYE1Lw==');
define('NONCE_KEY',        'hRp1VXoiWR2ci9p0ehy1kawX+lcjNW2JQK53xSNcTgqRM8AY5oHxI3n2gmLiDh751dIasUMIrn2voxYozKXfjQ==');
define('AUTH_SALT',        'LjLCE32TjDIPkHSn0EHEG+DHadFlvyZf3wGX/9enL1fYym/DtCcJkbtVAcGaTw03O0xv9J/ymHOZeB+4Q6DpLw==');
define('SECURE_AUTH_SALT', 'zKohqsDL6HzDxBWhLvE1O/L6SrcMECNjQ4GZMU1CvHKWO+RCvJK89uljs5g9T/nVvdkqz8SOKc4lUS4Sv2l/4g==');
define('LOGGED_IN_SALT',   'ayB5pQNite/XPqamtOPelQUMrHZBrsIHg31AZkbulkTJABVg15Kc/MIA0ZLusn6vSJrhzmktZr6vK4EOlKhKqQ==');
define('NONCE_SALT',       'n2XM2B5btUCk0xSHstoegWPILTHeBlan4ZDrxpV9qtIPzYB1+QVBqMrKJU/pAdbEczwx2ASls10DiGPd9oANqg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

@ini_set( 'upload_max_filesize' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'memory_limit', '256M' );
@ini_set( 'max_execution_time', '300' );
@ini_set( 'max_input_time', '300' );
